﻿using System;
using System.IO;
using System.Linq;
using RtspCapture.Configuration;
using RtspCapture.Core;
using System.Threading;

namespace RtspCapture
{
  static class Program
  {
    private const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
    private const int CheckPreviousRecordsDays = 31;
    private static volatile bool _isGeneratingVideo;

    static void Main()
    {
      var configurationReader = new ConfigurationReader(@"Configuration\configuration.json");
      var cameras = configurationReader.ReadCamerasConfig();

      var memoryCleanThread = new TimelyThread(() =>
      {
        Console.WriteLine($"{DateTime.Now.ToString(DateTimeFormat)} Killing ffmpeg processes");
        WinCommandExecutor.KillProcesses("ffmpeg");
      }, configurationReader.MemoryCleanInterval);

      memoryCleanThread.StartExecution();

      foreach (var camera in cameras)
      {
        var timelyThread = new TimelyThread(() =>
        {
          if (!Directory.Exists(camera.JpegFolder))
          {
            Console.WriteLine($"{DateTime.Now.ToString(DateTimeFormat)} Creating new directory {camera.JpegFolder}");
            Directory.CreateDirectory(camera.JpegFolder);
          }

          var snapshotCommand = camera.TakeSnapshotToJpegFileCommand();

          Console.WriteLine($"{DateTime.Now.ToString(DateTimeFormat)} Taking snapshot for camera {camera.Name}");
          WinCommandExecutor.ExecuteCommandAsync(snapshotCommand.FilePath, snapshotCommand.Arguments);

          foreach (var day in Enumerable.Range(1, CheckPreviousRecordsDays))
          {
            var date = DateTime.Now.AddDays(-day);

            if (!_isGeneratingVideo && camera.CreateVideo && !camera.VideoExists(date))
            {
              var createVideoCommand = camera.CreateVideoFromJpegCommand(date);

              memoryCleanThread.Pause();

              if (configurationReader.SingleThreadVideoGenerationMode)
              {
                _isGeneratingVideo = true;
              }

              Console.WriteLine($"{DateTime.Now.ToString(DateTimeFormat)} Started generating video for camera {camera.Name}");
              WinCommandExecutor.ExecuteCommandSync(createVideoCommand.FilePath, createVideoCommand.Arguments);
            }

            if (camera.CanRemoveJpeg(date))
            {
              Console.WriteLine($"{DateTime.Now.ToString(DateTimeFormat)} Removing JPEG for {camera.Name}");
              camera.RemoveJpeg(date);
              _isGeneratingVideo = false;
              memoryCleanThread.Resume();
            }

            if (camera.CanRemoveFolder(date))
            {
              Console.WriteLine($"{DateTime.Now.ToString(DateTimeFormat)} Removing records for {camera.Name} {date.Date.ToString("d")}");
              camera.RemoveFolder(date);
            }
          }        
        }, camera.CaptureDelaySeconds);

        timelyThread.StartExecution();
        Thread.Sleep(5000);
      }

      ConsoleKeyInfo input;
      do
      {
        input = Console.ReadKey();
      } while (input.Key != ConsoleKey.Escape);
    }
  }
}