﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RtspCapture.Configuration
{
  public class Camera
  {
    private const string FilesTxtName = "_files.txt";
    private const string FileNameDatePattern = "yyyyMMdd";
    private const string VideoExtension = ".mp4";

    private string _jpegFolder;
    public string Name { get; set; }
    public string RtspLink { get; set; }
    public int CaptureDelaySeconds { get; set; }
    public int KeepVideoForDays { get; set; }
    public bool CreateVideo { get; set; }
    public string FfmpegExecutablePath { get; set; }

    public string JpegFolder
    {
      get => _jpegFolder + DateTime.Now.ToString(FileNameDatePattern) + "\\";

      set => _jpegFolder = value;
    }

    public Command TakeSnapshotToJpegFileCommand()
    {
      var arguments = $@"-i {RtspLink} -f image2 -vframes 1 {JpegFolder + GenerateFileName(".jpeg")} -q:v 1 -y";

      return new Command {FilePath = FfmpegExecutablePath, Arguments = arguments};
    }

    public Command CreateVideoFromJpegCommand(DateTime date)
    {
      CreateTxtWithFileNames(date);

      var arguments =
        $@"-r 5 -f concat -safe 0 -i {GetJpegFolderForDate(date) + FilesTxtName} -c:v libx264 -pix_fmt yuv420p {GetJpegFolderForDate(date) + date.ToString(FileNameDatePattern)}{VideoExtension} -preset ultrafast -tune zerolatency";

      return new Command { FilePath = FfmpegExecutablePath, Arguments = arguments };
    }

    public bool VideoExists(DateTime date)
    {
      if (!Directory.Exists(_jpegFolder + date.ToString(FileNameDatePattern)))
      {
        return true;
      }

      return File.Exists(_jpegFolder + date.ToString(FileNameDatePattern) + "\\" + date.ToString(FileNameDatePattern) + VideoExtension);
    }

    public bool CanRemoveJpeg(DateTime date)
    {
      var result = false;
      var fileName = _jpegFolder + date.ToString(FileNameDatePattern) + "\\" + date.ToString(FileNameDatePattern) +
                     VideoExtension;

      if (File.Exists(fileName) && Directory.GetFiles(_jpegFolder + date.ToString(FileNameDatePattern) + "\\", "*.jpeg").Length > 0)
      {
        if (new FileInfo(fileName).LastWriteTime.AddMinutes(5) <= DateTime.Now)
        {
          result = true;
        }
      }

      return result;
    }

    public bool CanRemoveFolder(DateTime date)
    {
      var result = false;
      var folderName = _jpegFolder + date.ToString(FileNameDatePattern);
      var canRemoveForDate = date.AddDays(KeepVideoForDays).Date < DateTime.Now.Date;

      if (Directory.Exists(folderName) && canRemoveForDate)
      {
        result = true;
      }

      return result;
    }

    public void RemoveJpeg(DateTime date)
    {
      var fileNames = GetJpegFileNamesInFolder(GetJpegFolderForDate(date));

      foreach (var file in fileNames)
      {
        File.Delete(file);
      }
    }

    public void RemoveFolder(DateTime date)
    {
      var folderName = _jpegFolder + date.ToString(FileNameDatePattern);

      if (Directory.Exists(folderName))
      {
        Directory.Delete(folderName, true);
      }
    }

    private string GetJpegFolderForDate(DateTime date)
    {
      return _jpegFolder + date.ToString(FileNameDatePattern) + "\\";
    }

    private string GenerateFileName(string extension)
    {
      return DateTime.Now.ToString($"{FileNameDatePattern}HHmmss") + extension;
    }

    private List<string> GetJpegFileNamesInFolder(string path)
    {
      var result = Directory.EnumerateFiles(path, "*.jpeg").ToList();

      return result;
    }

    private void CreateTxtWithFileNames(DateTime date)
    {
      var path = GetJpegFolderForDate(date) + FilesTxtName;
      var fileNames = GetJpegFileNamesInFolder(GetJpegFolderForDate(date));

      File.Delete(path);

      using (var streamWriter = File.CreateText(path))
      {
        foreach (var file in fileNames)
        {
          streamWriter.WriteLine("file " + file.Replace(@"\", @"\\"));
        }
      }
    }
  }
}
