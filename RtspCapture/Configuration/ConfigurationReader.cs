﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace RtspCapture.Configuration
{
  public class ConfigurationReader
  {
    private string _configurationFilePath { get; }
    private IConfigurationRoot _configuration { get; }

    public int MemoryCleanInterval
    {
      get
      {
        var result = _configuration.GetSection("clearMemoryIntervalSeconds").Value;

        return Convert.ToInt32(result);
      }
    }

    public bool SingleThreadVideoGenerationMode
    {
      get
      {
        var result = _configuration.GetSection("singleThreadVideoGenerationMode").Value;

        return Convert.ToBoolean(result);
      }
    }

    public ConfigurationReader(string configurationFilePath)
    {
      _configurationFilePath = configurationFilePath;

      var builder = new ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile(_configurationFilePath, true, true);

      _configuration = builder.Build();
    }

    public List<Camera> ReadCamerasConfig()
    {
      var result = new List<Camera>();
      var cams = _configuration.GetSection("cameras").GetChildren();

      foreach (var cam in cams)
      {
        result.Add(new Camera
        {
          Name = cam["name"],
          RtspLink = cam["rtspLink"],
          JpegFolder = cam["jpegFolder"],
          CaptureDelaySeconds = Convert.ToInt32(cam["captureDelaySeconds"]),
          KeepVideoForDays = Convert.ToInt32(cam["keepVideoForDays"]),
          CreateVideo = Convert.ToBoolean(cam["createVideo"]),
          FfmpegExecutablePath = _configuration.GetSection("ffmpegExecutablePath").Value
        });
      }

      return result;
    }
  }
}
