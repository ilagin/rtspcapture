﻿namespace RtspCapture.Configuration
{
  public class Command
  {
    public string FilePath { get; set; }
    public string Arguments { get; set; }
  }
}
