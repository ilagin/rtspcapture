﻿using System;
using System.Threading;

namespace RtspCapture.Core
{
  public class TimelyThread
  {
    private readonly Thread _thread;
    private volatile bool _paused;

    public TimelyThread(Action action, int delaySeconds)
    {
      _thread = new Thread(() =>
      {
        action();

        var timer = new System.Timers.Timer(delaySeconds * 1000);

        timer.Elapsed += (sender, args) =>
        {
          if (!_paused)
          {
            action();
          }
        };

        timer.Start();
      });
    }

    public void StartExecution()
    {
      _thread.Start();
    }

    public void Pause()
    {
      _paused = true;
    }

    public void Resume()
    {
      _paused = false;
    }
  }
}
