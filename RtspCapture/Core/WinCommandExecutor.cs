﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace RtspCapture.Core
{
  public static class WinCommandExecutor
  {
    public static void ExecuteCommandSync(string command, string arguments)
    {
      try
      {
        var procStartInfo =
          new ProcessStartInfo(command)
          {
            Arguments = arguments,
            RedirectStandardOutput = true,
            UseShellExecute = false,
            CreateNoWindow = true
          };
        var proc = new Process { StartInfo = procStartInfo };

        proc.Start();
      }
      catch (Exception exception)
      {
        Console.WriteLine(exception.Message);
      }
    }

    public static void ExecuteCommandAsync(string command, string arguments)
    {
      try
      {
        Thread objThread = new Thread(() => {ExecuteCommandSync(command, arguments);})
        {
          IsBackground = true,
          Priority = ThreadPriority.AboveNormal
        };

        objThread.Start();
      }
      catch (ThreadStartException exception)
      {
        Console.WriteLine(exception.Message);
      }
      catch (ThreadAbortException exception)
      {
        Console.WriteLine(exception.Message);
      }
      catch (Exception exception)
      {
        Console.WriteLine(exception.Message);
      }
    }

    public static void KillProcesses(string processName)
    {
      var processCopies = Process.GetProcesses().Where(x => x.ProcessName.Contains(processName));

      foreach (var process in processCopies)
      {
        process.Kill();
      }
    }
  }
}
